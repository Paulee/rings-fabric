package pw.paul;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pw.paul.item.PiglinRingItem;
import pw.paul.item.RingItem;

public final class RingsMod implements ModInitializer {

  private static final Logger LOGGER = LogManager.getLogger("RINGS");

  public static final Item PIGLIN_RING = new PiglinRingItem();

  @Override
  public void onInitialize() {
    Registry.register(Registry.ITEM, new Identifier("rings", "ring"),
      new RingItem()
    );

    Registry.register(Registry.ITEM, new Identifier("rings", "piglin_ring"),
      PIGLIN_RING
    );

    LOGGER.info("Loaded Rings modification by Paul");
  }
}
