package pw.paul.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class RingItem extends Item {

  public RingItem() {
    super(new Item.Settings().fireproof().maxCount(1).group(ItemGroup.TOOLS));
  }

  @Override
  public boolean isEnchantable(ItemStack stack) {
    return false;
  }

  @Override
  public boolean isDamageable() {
    return false;
  }
}
