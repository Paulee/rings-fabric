package pw.paul.item;

import java.util.List;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public final class PiglinRingItem extends RingItem {

  @Override
  public void appendTooltip(
    ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context
  ) {
    tooltip.add(new TranslatableText("rings.description"));
  }

  @Override
  public boolean hasGlint(ItemStack stack) {
    return true;
  }
}
