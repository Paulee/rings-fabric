package pw.paul.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.PiglinBrain;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.entity.EntityPredicates;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import pw.paul.RingsMod;

@Mixin(PiglinBrain.class)
public abstract class PiglinBrainMixin {

  @Inject(method = "shouldAttack", at = @At("HEAD"), cancellable = true)
  private static void injectShouldAttack(
    LivingEntity target,
    CallbackInfoReturnable<Boolean> callbackInfoReturnable
  ) {
    callbackInfoReturnable
      .setReturnValue(
        EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR.test(target) || hasRing(target));
  }

  @Inject(method = "wearsGoldArmor", at = @At("HEAD"), cancellable = true)
  private static void injectWearsGoldArmor(
    LivingEntity livingEntity,
    CallbackInfoReturnable<Boolean> callbackInfoReturnable
  ) {
    callbackInfoReturnable.setReturnValue(hasRing(livingEntity));
  }

  private static boolean hasRing(LivingEntity livingEntity) {
    if (livingEntity instanceof PlayerEntity) {
      PlayerEntity playerEntity = (PlayerEntity) livingEntity;
      return playerEntity.inventory.contains(new ItemStack(RingsMod.PIGLIN_RING));
    }
    return false;
  }

}
